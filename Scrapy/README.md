# Scrapy

### Estado actual de compilación
[![Linux](https://img.shields.io/circleci/project/github/conda-forge/scrapy-feedstock/master.svg?label=Linux)](https://circleci.com/gh/conda-forge/scrapy-feedstock)
[![OSX](https://img.shields.io/travis/conda-forge/scrapy-feedstock/master.svg?label=macOS)](https://travis-ci.org/conda-forge/scrapy-feedstock)
[![Windows](https://img.shields.io/appveyor/ci/conda-forge/scrapy-feedstock/master.svg?label=Windows)](https://ci.appveyor.com/project/conda-forge/scrapy-feedstock/branch/master)

## Instalación 🔧
Scrapy se ejecuta en Python 2.7 y Python 3.4 o superior bajo CPython (implementación predeterminada de Python) y PyPy (comenzando con PyPy 5.9). (https://doc.scrapy.org/en/latest/intro/install.html#intro-install)

### Anaconda
Si se realiza la instalación desde Anaconda o minianaconda:
```bash
conda install -c conda-forge scrapy
```
### Python
con la instalación de paquetes de Python, puede instalar Scrapy y sus dependencias desde PyPI con:

```bash
pip instalar Scrapy
```
### Entorno virtual (recomendado)
Se recomienda utilizar un entorno virtual para evitar problemas con las dependencias del sistema operativo.

```bash
$ [sudo] pip install virtualenv
```
Una vez que haya creado un virtualenv, puede instalar scrapy dentro de él con pip, como cualquier otro paquete de Python. 

**Entorno en Windows**
```bash
conda install -c conda-forge scrapy

```

**Linux** necesita instalar estas dependencias:
```bash
sudo apt-get install python-dev python-pip libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev

```
```bash
sudo apt-get install python3 python3-dev
```

```bash
pip install scrapy
```

**Mac OS X**
Scrapy requiere la presencia de un compilador de C y encabezados de desarrollo. 

```bash
xcode-select --install
```
```bash
echo "export PATH=/usr/local/bin:/usr/local/sbin:$PATH" >> ~/.bashrc
```

No use python del sistema, instale una versión nueva y actualizada que no entre en conflicto con el resto de su sistema. <br/>		
* Instale homebrew siguiendo las instrucciones en https://brew.sh/

* Actualice su variable PATH para indicar que los paquetes homebrew <del></del>ben usarse antes que los paquetes del sistema (cambie .bashrc a .zshrc de forma acorde si está usando zsh como shell predeterminado): <br/>
Vuelva a cargar <code>.bashrc </code> para asegurarse de que los cambios hayan tenido lugar:

```bash
source ~/.bashrc
```
* Instalar python

```bash
brew install python
```
* Actualizar python

```bash
brew update; brew upgrade python
```
## Creando un proyecto :hammer:
1. Se configura un nuevo proyecto
```bash
scrapy startproject tutorial
```
2. Quedando con la siguiente estructura:
```bash
tutorial/
    scrapy.cfg            # desplegar archivo de configuración

    tutorial/             # módulo de Python del proyecto, importará su código desde aquí
        __init__.py

        items.py          # archivo de definición de elementos del proyecto

        middlewares.py    # archivos middlewares del proyecto

        pipelines.py      # archivo de pipelines del proyecto

        settings.py       # archivo de configuraciones del proyecto

        spiders/          # un directorio donde más tarde pondrás tus spiders
            __init__.py
```
### Ejemplo :memo:
**Método start_requests**
Para scrapring necesitamos un Spider. Los spider definen cómo se raspará un determinado sitio. 

En lugar de implementar un método <code> start_requests () </code> que genere objetos de scrapy. Solicite desde URL, solo puede definir un atributo de clase <code> start_urls </code> con una lista de URL. Esta lista será utilizada por la implementación predeterminada de <code> start_requests () </code> para crear las solicitudes iniciales de su Spider.


```python
import scrapy

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'http://quotes.toscrape.com/page/1/',
        'http://quotes.toscrape.com/page/2/',
    ]

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'quotes-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)

```

## Extrayendo datos :inbox_tray:

Usando el shell, puede intentar seleccionar elementos usando CSS con el objeto de respuesta:
```bash
scrapy shell 'http://quotes.toscrape.com/page/1/'
```
```shell
[ ... Scrapy log here ... ]
2016-09-19 12:09:27 [scrapy.core.engine] DEBUG: Crawled (200) <GET http://quotes.toscrape.com/page/1/> (referer: None)
[s] Available Scrapy objects:
[s]   scrapy     scrapy module (contains scrapy.Request, scrapy.Selector, etc)
[s]   crawler    <scrapy.crawler.Crawler object at 0x7fa91d888c90>
[s]   item       {}
[s]   request    <GET http://quotes.toscrape.com/page/1/>
[s]   response   <200 http://quotes.toscrape.com/page/1/>
[s]   settings   <scrapy.settings.Settings object at 0x7fa91d888c10>
[s]   spider     <DefaultSpider 'default' at 0x7fa91c8af990>
[s] Useful shortcuts:
[s]   shelp()           Shell help (print this help)
[s]   fetch(req_or_url) Fetch request (or URL) and update local objects
[s]   view(response)    View response in a browser
>>>

```
<br/>

* Usando el shell, puede intentar seleccionar elementos usando CSS con el objeto de respuesta:
```shell
>>> response.css('title')
[<Selector xpath='descendant-or-self::title' data='<title>Quotes to Scrape</title>'>]
```

Para extraer el texto del título anterior, puedes hacer:
```shell
>>> response.css('title::text').extract()
['Quotes to Scrape']
```
Hay dos cosas que se deben tener en cuenta: una es que hemos agregado <code> :: texto </code> a la consulta de CSS, lo que significa que queremos seleccionar solo los elementos de texto directamente dentro del elemento <code> title </code> . Si no especificamos <code> :: texto </code>, obtendríamos el elemento de título completo, incluidas sus etiquetas:
```shell
>>> response.css('title').extract()
['<title>Quotes to Scrape</title>']
```

El resultado de ejecutar response.css ('título') es un objeto parecido a una lista llamado SelectorList, que representa una lista de objetos Selector que envuelven elementos XML / HTML y le permiten ejecutar más consultas para concretar la selección o el extracto los datos

Para extraer el texto del título anterior, puedes hacer:

```shell
>>> response.css('title::text')[0].extract()
'Quotes to Scrape'
```

**HTML** <br/>
Teniendo la estructura en el HTML de  http://quotes.toscrape.com se van identificando los elementos CSS para el Scraping.
```html
<div class="quote">
    <span class="text">“The world as we have created it is a process of our
    thinking. It cannot be changed without changing our thinking.”</span>
    <span>
        by <small class="author">Albert Einstein</small>
        <a href="/author/Albert-Einstein">(about)</a>
    </span>
    <div class="tags">
        Tags:
        <a class="tag" href="/tag/change/page/1/">change</a>
        <a class="tag" href="/tag/deep-thoughts/page/1/">deep-thoughts</a>
        <a class="tag" href="/tag/thinking/page/1/">thinking</a>
        <a class="tag" href="/tag/world/page/1/">world</a>
    </div>
</div>
```

```shell
>>> title = quote.css("span.text::text").extract_first()
>>> title
'“The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.”'
>>> author = quote.css("small.author::text").extract_first()
>>> author
'Albert Einstein'
```

```shell
>>> response.css('title::text').extract()
['Quotes to Scrape']
```
* Para ejecutar

```bash
scrapy crawl QuotesSpider
```

Aquí está el código para que un Spider siga los enlaces a las preguntas más votadas en [StackOverflow](Ejemplos/StackOverflowSpider.py) y raspe algunos datos de cada página.

## Referencias :pushpin:
* https://doc.scrapy.org/en/latest/intro/install.html#intro-install
* https://doc.scrapy.org/en/latest/intro/overview.html
