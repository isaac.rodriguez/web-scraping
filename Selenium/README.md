# Selenium WebDriver
### Versiones soportadas
Python 2.7, 3.4+

## Introducción
A algunos sitios web no les gusta ser Scrapiados. En estos casos, es posible que necesite simular a un usuario real que trabaja con un navegador. Selenium lanza y controla un navegador web.

## Instalación 🔧
Los enlaces de Selenium Python proporcionan una API conveniente para acceder a Selenium WebDrivers como Firefox, Ie, Chrome, Remote, etc. Las versiones actuales de Python compatibles son 2.7, 3.5 y superiores. (https://selenium-python.readthedocs.io/index.html)

### Python

Puede descargar enlaces de Python para Selenium desde la página de PyPI para el paquete de selenium. Sin embargo, un mejor enfoque sería utilizar pip para instalar el paquete de selenio. Python 3.6 tiene pip disponible en la biblioteca estándar. Usando pip, puedes instalar selenio:

```bash
pip install selenium
```
Puede considerar usar virtualenv para crear entornos Python aislados. 
```bash
$ virtualenv webscraping_example
```
A continuación, instale la dependencia en su virtualenv ejecutando el siguiente comando en el terminal:
```bash
$ (webscraping_example) pip install -r setup.py
```

### Drivers

Selenium requiere un controlador para interactuar con el navegador elegido. <br> Firefox, por ejemplo, requiere <code> [geckodriver](#https://github.com/mozilla/geckodriver/releases) </code> , que debe instalarse antes de poder ejecutar los siguientes ejemplos. 
Asegúrese de que esté en su RUTA PATH:
```bash
 /usr/bin o /usr/local/bin.
```
Otros navegadores compatibles tendrán sus propios controladores disponibles. Los enlaces a algunos de los controladores de navegador más populares son:

| Navegador       | Enlace          |
| ------------- |:-------------:| 
| Chrome     |  https://sites.google.com/a/chromium.org/chromedriver/downloads | 
| Firefox      | https://github.com/mozilla/geckodriver/releases     | 
| Edge | https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/      |
| Safari | https://webkit.org/blog/6900/webdriver-support-in-safari-10/      | 


**Windows**
1. Instalar Python 3.6 utilizando el MSI disponible en la página de descarga de python.org.
2. Inicie un indicador de comando usando el programa cmd.exe y ejecute el comando pip como se indica a continuación para instalar Selenium.

```bash
C: \ Python35 \ Scripts \ pip.exe install selenium
```
3. Ahora puedes ejecutar tus scripts de prueba usando Python. 
<br/> Por ejemplo, si ha creado un script basado en Selenium y lo ha guardado en C: \ my_selenium_script.py, puede ejecutarlo de la siguiente manera:
```bash
C: \ Python35 \ python.exe C: \ my_selenium_script.py
```
**Linux**
Suponiendo que la ruta ~ / .local / bin está en su RUTA de ejecución, aquí se explica cómo instalaría Firefox WebDriver, llamado geckodriver, en una máquina con Linux:
1. Desde la terminal primero se debe insalar el paquete con el driver específico en este caso gecodriver:

```bash
$ wget https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-linux64.tar.gz
```

```bash
$ tar xvfz geckodriver-v0.19.1-linux64.tar.gz
```

```bash

$ mv geckodriver ~/.local/bin
```
Una vez descargado el paquete se puede continuar con la instalación de Selenium


```bash

$ pip install selenium
```

### Descargando el servidor Selenium

Solo es necesario en caso de que se quiera utilizar el [WebDriver remoto](#https://selenium-python.readthedocs.io/getting-started.html#selenium-remote-webdriver)
Para usar el WebDriver remoto, debe tener el servidor Selenium en ejecución. Para ejecutar el servidor, use este comando:
```bash
java -jar selenium-server-standalone-2.x.x.jar
```
> Se recomienda Java Runtime Environment (JRE) 1.6 o una versión más reciente para ejecutar el servidor Selenium.

**Ejemplo**
```python
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.CHROME)

driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.OPERA)

driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)
```

## Interactuando con la página
Se requiere identificar los elementos de la página HTML.
WebDriver ofrece varias formas de encontrar elementos. Por ejemplo, dado un elemento definido como:
```html
<input type="text" name="passwd" id="passwd-id" />
```
Usted podría encontrarlo usando cualquiera de:

```python
element = driver.find_element_by_id("passwd-id")
element = driver.find_element_by_name("passwd")
element = driver.find_element_by_xpath("//input[@id='passwd-id']")
```
> Si hay más de un elemento que coincide con la consulta, solo se devolverá el primero.
> Si no se encuentra nada, se generará una excepción NoSuchElementException.

¿Qué puedes hacer con eso? En primer lugar, es posible que desee ingresar texto en un campo de texto:
```python
element.send_keys("some text")
```
Puedes simular presionando las teclas de flecha usando la clase "Teclas".
> Es posible llamar a send_keys en cualquier elemento.

```python
element.send_keys(" and some", Keys.ARROW_DOWN)
```

## Extrayendo datos :inbox_tray:

1. Importar  módulo que provee la implementación.

```python
from selenium import webdriver 
```
2. Importación de llaves
El módulo selenium.webdriver proporciona todas las implementaciones de WebDriver. Las implementaciones soportadas actualmente de WebDriver son Firefox, Chrome, Ie y Remote. 
> La clase Keys proporciona teclas en el teclado como RETURN, F1, ALT etc.
```python
from selenium.webdriver.common.keys import Keys
```
3. Se crea instancia de Firefox WebDriver.
```python
driver = webdriver.Firefox() 	
```
4. Método driver.get
El método driver.get navegará a una página dada por la URL. WebDriver esperará hasta que la página se haya cargado por completo (es decir, se haya activado el evento "onload") antes de devolver el control a su prueba o script. 
> Si la página utiliza una gran cantidad de AJAX en la carga, es posible que WebDriver no sepa cuándo se ha cargado completamente.

```python
driver.get("http://www.python.org")	
```
5. La siguiente línea es una afirmación para confirmar que el título tiene la palabra "Python" en ella.

```python
assert "Python" in driver.title
```
6. WebDriver ofrece varias formas de encontrar elementos. Uno de los enfoques es utilizar los métodos find_element_by_ *. Los métodos más utilizados son:
```bash
* ind_element_by_id.
* find_element_by_name.
* find_element_by_xpath.
* find_element_by_link_text.
* find_element_by_partial_link_text.
* find_element_by_tag_name.
* find_element_by_class_name.
* find_element_by_css_selector.
```
por ejemplo, seleccionar el elemento llamado "q"
```python
elem = driver.find_element_by_name("q")
```

7. A continuación, le enviaremos las claves, esto es similar a ingresar las teclas con su teclado. Las claves especiales se pueden enviar usando la clase Keys importada de selenium.webdriver.common.keys:

```python
elem.send_keys("selenium")
elem.send_keys(Keys.RETURN)
```
8. Después del envío de la página, debe ser contactado en el sitio de Google:

```python
assert "Google" in driver.title
```
9. Finalmente, se cierra la ventana del navegador. 

```python
driver.close()
```
```python
from selenium import webdriver 						
from selenium.webdriver.common.keys import Keys

driver = webdriver.Firefox() 						
driver.get("http://www.python.org")
assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.send_keys("selenium")
elem.send_keys(Keys.RETURN)
assert "Google" in driver.title
driver.close()

```

### Ejemplo :memo:


1. Abrir un nuevo navegador Firefox
2. Cargar la página en la URL dada
```python
from selenium import webdriver

browser = webdriver.Firefox()
browser.get('http://seleniumhq.org/')
```
3. Búsqueda de "seleniumhq"
4. Cierra el navegador
```python
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Firefox()

browser.get('http://www.yahoo.com')
assert 'Yahoo' in browser.title

elem = browser.find_element_by_name('p')  # Find the search box
elem.send_keys('seleniumhq' + Keys.RETURN)

browser.quit()

```


Búsqueda web básica a través de [DuckDuckGo](#https://duckduckgo.com/).

```python
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
opts = Options()
opts.set_headless()
assert opts.headless  
browser = Firefox(options=opts)
browser.get('https://duckduckgo.com')
```
En este momento, desea obtener un formulario de búsqueda para poder enviar una consulta. Al inspeccionar la página de inicio de DuckDuckGo, encontrará que el elemento de búsqueda <input> tiene un atributo de identificación "<code >search_form_input_hputepage </code>.
```python
search_form = browser.find_element_by_id('search_form_input_homepage')
search_form.send_keys('real python')
search_form.submit()
```
Encontró el formulario de búsqueda, usó el método send_keys para completarlo y luego el método para realizar su búsqueda de "Real Python". Puedes ver el resultado 
```python
results = browser.find_elements_by_class_name('result')
print(results[0].text)
```

Aquí está el código para que mediante Selenium siga los enlaces a las preguntas más votadas en [StackOverflow](Ejemplos/StackOverflowSelenium.py) y raspe algunos datos de la página.

## Referencias :pushpin:
* https://pypi.org/project/selenium/
* https://doc.scrapy.org/en/latest/intro/overview.html
* https://realpython.com/modern-web-automation-with-python-and-selenium/
* https://gist.github.com/daemianmack/1099713