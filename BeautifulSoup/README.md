# BeautifulSoup4
### Estado actual de compilación
[![Documentation Status](https://readthedocs.org/projects/beautiful-soup-4/badge/?version=latest)](https://beautiful-soup-4.readthedocs.io/en/latest/?badge=latest)

## Introducción
Beautiful Soup es una biblioteca para extraer datos de archivos HTML y XML. Proporciona formas de navegar, buscar y modificar árboles de análisis.
El paquete Beautiful Soup se utiliza para extraer datos de archivos html. El nombre de la biblioteca de Beautiful Soup es bs4 que significa Beautiful Soup, versión 4.

## Instalación 🔧

### Anaconda
Si se realiza la instalación desde Anaconda o minianaconda:
```bash
conda install -c anaconda beautifulsoup4 
```
* Licencia: MIT


### Python
**Instalación de Python**
```shell
$ python setup.py install
```
Se puede instalar usando <code> pip </code>, el nombre del paquete beautifulsoup4 que funciona para Python 2 y Python 3.

```shell
$ pip install beautifulsoup4
```
```shell
$ pip install lxml
$ pip install html5lib
```
**Parser**
Los diferentes parsers son: <code> html.parser </code>, <code>lxml </code>, y <code>html5lib </code>.
<br/>
[lxml](https://lxml.de/)
```bash
lxml es la biblioteca más rica en funciones y fácil de usar para procesar XML y HTML en el lenguaje Python.
```
[html5lib](https://github.com/html5lib/html5lib-python)
```bash
html5lib es una biblioteca de python puro para analizar HTML. Está diseñado para cumplir con la especificación HTML WHATWG, como lo implementan todos los principales navegadores web.

```
Ejemplo
```bash

import html5lib
document = html5lib.parse("<p>Hello World!")

```
De forma predeterminada, el documento será una instancia del elemento xml.etree.
![](imagenes/capturaCodigo/html5lib.png)<br /><br />

## Extrayendo datos :inbox_tray:

Se muestran a continuación los pasos necesarios para realizar Web Scraping.

**Ejemplo 1**
1. **Conocer el problema** y **saber qué datos** necesitamos recolectar.

2. **Analizar la estructura del HTML** de la web para ver de qué forma podemos extraer su contenido.

3. Identificar **qué etiquetas** contienen la información que necesitamos.

5. Realizar la **petición HTTP** a la página web para obtener su HTML. 

Esto se realiza mediante una petición <code> request.get </code>
```python
url = "https://jarroba.com/"
# Realizamos la petición a la web
req = requests.get(url)
```

6. El objeto <code> req </code> almacena los datos de la petición HTTP (cabeceras, cookies) obtenemos el Status Code y el HTML (como un string) de la web. Esto lo hacemos de la siguiente forma: 
```python
statusCode = req.status_code
htmlText = req.text
```

7. Definimos la url para obtener el Status Code y el HTML. En segundo lugar comprobamos que el "status code" es igual a 200 y si es así obtenemos en un objeto de la clase "BeautifulSoup", pasándole el HTML de la web. 
```python
# Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
html = BeautifulSoup(req.text, "html.parser")

# Obtenemos todos los divs donde estan las entradas
entradas = html.find_all('div',{'class':'col-md-4 col-xs-12'})
```

8. En este caso utilizamos el método "find_all()" que lo que hace es coger todos los fragmentos del HTML que correpondan a una etiqueta div.

```python
# Recorremos todas las entradas para extraer el título, autor y fecha
for i,entrada in enumerate(entradas):
    # Con el método "getText()" no nos devuelve el HTML
    titulo = entrada.find('span', {'class' : 'tituloPost'}).getText()
    # Sino llamamos al método "getText()" nos devuelve también el HTML
    autor = entrada.find('span', {'class' : 'autor'})
    fecha = entrada.find('span', {'class' : 'fecha'}).getText()

    # Imprimo el Título, Autor y Fecha de las entradas
    print "%d - %s  |  %s  |  %s" %(i+1,titulo,autor,fecha)
```
Al ejecutarlo:
```shell
1 - Arquitectura Android – ART vs Dalvik  |  Por: Ramón Invarato  |  17-may-2015
2 - Scraping en Java (JSoup), con ejemplos  |  Por: Ricardo Moya  |  12-may-2015
3 - Diccionario en python, con ejemplos  |  Por: Ricardo    Moya  |  05-may-2015
4 - DataSet de resultados de partidos de fútbol para su predicción (Machine Learning)  |  Por: Ricardo  Moya  |  26-abr-2015
5 - Intalar Ubuntu en una máquina virtual con VirtualBox (Video)  |  Por: Ricardo   Moya  |  15-abr-2015
6 - Lectura y Escritura de ficheros en Java, con ejemplos  |  Por: Ricardo  Moya  |  08-abr-2015
7 - List en Python, con ejemplos  |  Por: Ricardo   Moya  |  06-abr-2015```
8 - Python MongoDB Driver (pymongo), con ejemplos (Video)  |  Por: R```icardo  Moya  |  23-mar-2015
9 - Java MongoDB Driver, con ejemplos (Video)  |  Por: Ricardo  Moya  |  18-mar-2015
```
<br/>

**Ejemplo 2**

El módulo urllib.request se utiliza para abrir direcciones URL. El paquete Beautiful Soup se utiliza para extraer datos de archivos html. 
![](imagenes/capturaCodigo/BeautifulSoup1.png)
<br />


Se debe especificar la URL que contiene el conjunto de datos y pasarlo a urlopen () para obtener el código HTML de la página
![](imagenes/capturaCodigo/BeautifulSoup2.png)
<br />



El siguiente paso es crear un objeto Beautiful Soup a partir del html. Esto se hace pasando el html a la función BeautifulSoup (). El paquete Beautiful Soup se usa para analizar el html, es decir, tomar el texto html sin procesar y dividirlo en objetos de Python.
![](imagenes/capturaCodigo/BeautifulSoup3.png)
<br />


El objeto de soup le permite extraer información interesante sobre el sitio web que está raspando, como obtener el título de la página como se muestra a continuación.
![](imagenes/capturaCodigo/BeautifulSoup4.png)
<br />


También puede obtener el texto de la página web e imprimirlo rápidamente para comprobar si es lo que espera.
![](imagenes/capturaCodigo/BeautifulSoup5.png)
<br />
Puede usar el método find_all () de soup para extraer etiquetas html útiles dentro de una página web. Los ejemplos de etiquetas útiles incluyen <a> para hipervínculos
![](imagenes/capturaCodigo/BeautifulSoup6.png)
<br />
### Probando con un ejemplo :memo:

1. **Conocer el problema** y **saber qué datos** necesitamos recolectar.

```
Se quiere analizar las distribuciones de las calificaciones de películas de IMBD.
```
* Se buscará las películas por año 2018, clasifiquemos las películas en la primer página por número de votos y luego pasemos a la página siguiente.

2. **Analizar la estructura del HTML** de la web para ver de qué forma podemos extraer su contenido.

![](imagenes/EjemploBeautiful/1_pagweb.png) <br/>
![](imagenes/EjemploBeautiful/2_pagweb.png) <br/>
![](imagenes/EjemploBeautiful/3_pagweb.png) <br/>
![](imagenes/EjemploBeautiful/3_pagweb1.png) <br/>
![](imagenes/EjemploBeautiful/4_pagweb.png) <br/>
![](imagenes/EjemploBeautiful/5_pagweb.png) <br/>


3. Identificar **qué etiquetas** contienen la información que necesitamos.

![](imagenes/EjemploBeautiful/1_inspeccionar.png) <br/>
![](imagenes/EjemploBeautiful/2_inspeccionar.png) <br/>
![](imagenes/EjemploBeautiful/3_inspeccionar.png) <br/>
![](imagenes/EjemploBeautiful/4_inspeccionar.png) <br/>
![](imagenes/EjemploBeautiful/5_inspeccionar.png) <br/>
![](imagenes/EjemploBeautiful/7_inspeccionar.png) <br/>

5. Realizar la **petición HTTP** a la página web para obtener su HTML. 
```python
from requests import get

url = 'http://www.imdb.com/search/title?release_date=2018&sort=num_votes,desc&page=1'
response = get(url)
print(response.text[:500]))

from bs4 import BeautifulSoup

html_soup = BeautifulSoup(response.text, 'html.parser')
type(html_soup)
```
![](imagenes/EjemploBeautiful/1_codigo.png)

6. Se gestiona y se van obteniendo los elementos necesarios para obtener la información.

```python
movie_containers = html_soup.find_all('div', class_ = 'lister-item mode-advanced')
first_movie = movie_containers[0]
```

![](imagenes/EjemploBeautiful/2_codigo.png) <br/>
![](imagenes/EjemploBeautiful/3_codigo.png) <br/>
![](imagenes/EjemploBeautiful/4_codigo.png) <br/>
![](imagenes/EjemploBeautiful/5_codigo.png) <br/>
![](imagenes/EjemploBeautiful/6_codigo.png) <br/>

<br/>

```python
#Listas para almacenar los datos raspados 
names = []
years = []
imdb_ratings = []
metascores = []
votes = []

# Extraer datos de contenedor de película individual
for container in movie_containers:

    # Si la película tiene Metascore, entonces extraiga:
    if container.find('div', class_ = 'ratings-metascore') is not None:

        # El nombre
        name = container.h3.a.text
        names.append(name)

        # El año
        year = container.h3.find('span', class_ = 'lister-item-year').text
        years.append(year)

        # Rating IMDB 
        imdb = float(container.strong.text)
        imdb_ratings.append(imdb)

        # Metascore
        m_score = container.find('span', class_ = 'metascore').text
        metascores.append(int(m_score))

        # Número de votos
        vote = container.find('span', attrs = {'name':'nv'})['data-value']
        votes.append(int(vote))
```

![](imagenes/EjemploBeautiful/7_codigo.png) <br/>

7. Mediante la librería <code> pandas </code> se permite guardar los datos obtenidos y comenzar un análisis sobre ellos.

```python
import pandas as pd
#Crear un DataFrame con los datos obtenidos
test_df = pd.DataFrame({'movie': names,
                       'year': years,
                       'imdb': imdb_ratings,
                       'metascore': metascores,
                       'votes': votes})
print(test_df.info())
test_df
```

![](imagenes/EjemploBeautiful/8_codigo.png) <br/>
![](imagenes/EjemploBeautiful/9_codigo.png) <br/>

8. Se requiere además ir navegando entre las diferentes páginas.

```python
# Redeclarar las listas para almacenar datos
names = []
years = []
imdb_ratings = []
metascores = []
votes = []

# Preparando el seguimiento del bucle
start_time = time()
requests = 0

# Por cada año en el intervalo 2017-2018.
for year_url in years_url:

    # Para cada página en el intervalo 1-4
    for page in pages:

        # Hacer una solicitud de obtención
        response = get('http://www.imdb.com/search/title?release_date=' + year_url + 
        '&sort=num_votes,desc&page=' + page, headers = headers)

        # Pausar el bucle
        sleep(randint(8,15))

         # Monitorear las solicitudes
        requests += 1
        elapsed_time = time() - start_time
        print('Request:{}; Frequency: {} requests/s'.format(requests, requests/elapsed_time))
        clear_output(wait = True)

        # Lanzar una advertencia para códigos de estado que no sean 200
        if response.status_code != 200:
            warn('Request: {}; Status code: {}'.format(requests, response.status_code))

        # Romper el ciclo si el número de solicitudes es mayor que el esperado
        if requests > 72:
            warn('Number of requests was greater than expected.')  
            break 

        # Analizar el contenido de la solicitud con BeautifulSoup
        page_html = BeautifulSoup(response.text, 'html.parser')

        # Seleccione todos los 50 contenedores de películas de una sola página
        mv_containers = page_html.find_all('div', class_ = 'lister-item mode-advanced')

         # Para cada película de estos 50
        for container in mv_containers:
             # Si la película tiene un Metascore, entonces:
            if container.find('div', class_ = 'ratings-metascore') is not None:

                # Scrape el nombre 
                name = container.h3.a.text
                names.append(name)

                # Scrape el año 
                year = container.h3.find('span', class_ = 'lister-item-year').text
                years.append(year)

                # Scrape el rating IMDB 
                imdb = float(container.strong.text)
                imdb_ratings.append(imdb)

                # Scrape el Metascore
                m_score = container.find('span', class_ = 'metascore').text
                metascores.append(int(m_score))

                # Scrape el número de votos
                vote = container.find('span', attrs = {'name':'nv'})['data-value']
                votes.append(int(vote))
```

![](imagenes/EjemploBeautiful/10_codigo.png) <br/>
![](imagenes/EjemploBeautiful/11_codigo.png) <br/>
![](imagenes/EjemploBeautiful/12_codigo.png) <br/>

9. Se vuelven a guardar los datos en un DataFrame

```python
movie_ratings = pd.DataFrame({'movie': names,
                              'year': years,
                              'imdb': imdb_ratings,
                              'metascore': metascores,
                              'votes': votes})
print(movie_ratings.info())
movie_ratings.head(10)
```

![](imagenes/EjemploBeautiful/13_codigo.png) <br/>
![](imagenes/EjemploBeautiful/14_codigo.png) <br/>
![](imagenes/EjemploBeautiful/15_codigo.png) <br/>

8. Mediante la librería de <code> matplotlib </code> se realiza la generación de gráficos.

```python
import matplotlib.pyplot as plt
%matplotlib inline

fig, axes = plt.subplots(nrows = 1, ncols = 3, figsize = (16,4))
ax1, ax2, ax3 = fig.axes

ax1.hist(movie_ratings['imdb'], bins = 10, range = (0,10)) # bin rango = 1
ax1.set_title('IMDB rating')

ax2.hist(movie_ratings['metascore'], bins = 10, range = (0,100)) # bin rango = 10
ax2.set_title('Metascore')

ax3.hist(movie_ratings['n_imdb'], bins = 10, range = (0,100), histtype = 'step')
ax3.hist(movie_ratings['metascore'], bins = 10, range = (0,100), histtype = 'step')
ax3.legend(loc = 'upper left')
ax3.set_title('The Two Normalized Distributions')

for ax in fig.axes:
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

plt.show()
```

![](imagenes/EjemploBeautiful/grafica.png) <br/>

#### Código ⚙
* Da clic aquí para [ir al código](Ejemplos/BeautifulSoup-Movies.ipynb)

#### Licencia
* Código: https://www.dataquest.io/blog/web-scraping-beautifulsoup/

## Pruebas ⚙
* Solo necesitas descargar e instalar Anaconda y correr Jupyter Notebook para ejecutar este ejemplo.

## Referencias :pushpin:
* https://readthedocs.org/projects/beautiful-soup-4/
* https://anaconda.org/anaconda/beautifulsoup4
* https://jarroba.com/scraping-python-beautifulsoup-ejemplos/
* https://www.datacamp.com/community/tutorials/web-scraping-using-python
